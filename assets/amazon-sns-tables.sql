SET FOREIGN_KEY_CHECKS=0;

# Volcado de tabla Device
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Device`;

CREATE TABLE `Device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(512) NOT NULL,
  `userId` int(11) NOT NULL,
  `amazonArn` varchar(512) DEFAULT NULL,
  `platform` varchar(512) NOT NULL,
  `appVersion` varchar(512) DEFAULT NULL,
  `unreadNotifications` int(11) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_fk` (`userId`),
  CONSTRAINT `userId_fk` FOREIGN KEY (`userId`) REFERENCES `ApplicationUser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Volcado de tabla NotificationStatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `NotificationStatus`;

CREATE TABLE `NotificationStatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `NotificationStatus` WRITE;
/*!40000 ALTER TABLE `NotificationStatus` DISABLE KEYS */;

INSERT INTO `NotificationStatus` (`id`, `name`)
VALUES
	(1,'Delivered'),
	(2,'Failed');

/*!40000 ALTER TABLE `NotificationStatus` ENABLE KEYS */;
UNLOCK TABLES;



# Volcado de tabla Notification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Notification`;

CREATE TABLE `Notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceId` int(11) NOT NULL,
  `historyId` int(11) DEFAULT NULL,
  `statusId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `deviceId_fk` (`deviceId`),
  KEY `statusId_fk` (`statusId`),
  CONSTRAINT `deviceId_fk` FOREIGN KEY (`deviceId`) REFERENCES `Device` (`id`),
  CONSTRAINT `statusId_fk` FOREIGN KEY (`statusId`) REFERENCES `NotificationStatus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Volcado de tabla Campaign
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Campaign`;

CREATE TABLE `Campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateAt` datetime NOT NULL DEFAULT NOW(),
  `name` varchar(50) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `active` tinyint(2) NOT NULL,
  `isLaunched` tinyint(2) NOT NULL,
  `launchedByAdmin` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Volcado de tabla Segment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Segment`;

CREATE TABLE `Segment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topicArn` varchar(150) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` varchar(100) NOT NULL,
  `language` varchar(2) NOT NULL DEFAULT 'es',
  `campaignId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
