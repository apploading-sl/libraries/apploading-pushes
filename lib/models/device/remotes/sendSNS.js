const PushUtils = require('../../../models-utils/PushUtils');

module.exports = function sendSNS({
  app,
  device,
  notificationData,
  alertMessage,
}) {
  const pushUtils = PushUtils();
  return new Promise((resolve, reject) => {
    app.sns.publish(
      {
        Message: pushUtils.createPayload(
          alertMessage,
          notificationData,
          device
        ),
        MessageStructure: 'json',
        TargetArn: device.amazonArn,
      },
      (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      }
    );
  });
};
