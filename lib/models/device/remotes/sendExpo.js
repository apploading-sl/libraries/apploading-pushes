const Promise = require('bluebird');
const { Expo } = require('expo-server-sdk');
const expo = new Expo();

module.exports = function sendExpo({ devices, notificationData, alertMessage }) {
  const messages = [];
  devices.forEach(({ amazonArn: expoToken }) => {
    if (Expo.isExpoPushToken(expoToken)) {
      messages.push({
        to: expoToken,
        sound: 'default',
        body: alertMessage,
        data: notificationData,
      });
    }
  });

  let chunks = expo.chunkPushNotifications(messages);
  return Promise.each(chunks, chunk => expo.sendPushNotificationsAsync(chunk));
};
