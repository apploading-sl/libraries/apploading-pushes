const sendSNS = require('./sendSNS');
const sendExpo = require('./sendExpo');

module.exports = function send(Device) {
  return sendFunction;

  function sendFunction({ device, notificationData, alertMessage }) {
    return device.platform == 'expo'
      ? sendExpo({
          devices: [device],
          notificationData,
          alertMessage,
        })
      : sendSNS({ app: Device.app, device, notificationData, alertMessage });
  }
};
