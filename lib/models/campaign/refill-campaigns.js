const Promise = require('bluebird');
const moment = require('moment');

module.exports = Campaign => {
  Campaign.on('attached', () => {
    const filter = { where: { dateAt: { gt: moment.utc().toDate() }, active: true } };
    Campaign.find(filter).then(campaigns => {
      const { loadCronJob } = require('./campaign-utils')(Campaign);
      campaigns = campaigns ? JSON.parse(JSON.stringify(campaigns)) : [];
      return Promise.each(campaigns, campaign => loadCronJob(campaign));
    });
  });
};
