const Promise = require('bluebird');

module.exports = (Campaign, ctx) => {
  const { options, instance, hookState } = ctx;

  if (options && options.accessToken && instance && hookState.languages) {
    const { sendCampaign, loadCronJob } = require('../../campaign-utils')(Campaign);
    const { id } = instance;
    return instance.launchNow
      ? sendCampaign(id)
      : loadCronJob(JSON.parse(JSON.stringify(instance)));
  } else return Promise.resolve();
};
