const Promise = require('bluebird');

module.exports = (Campaign, ctx) => {
  const { options, instance, hookState } = ctx;

  if (options && options.accessToken && instance && hookState.languages) {
    const { id: campaignId } = instance;
    const segments = hookState.languages.map(segment => {
      segment.campaignId = campaignId;
      return segment;
    });

    const { Segment } = Campaign.app.models;
    return Promise.each(segments, segment => Segment.upsert(segment));
  } else return Promise.resolve();
};
