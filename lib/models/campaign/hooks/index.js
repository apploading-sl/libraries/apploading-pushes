const BeforeSaveSegments = require('./before-save/save-segments');
const AfterSaveSegments = require('./after-save/save-segments');

const CheckToLaunch = require('./after-save/check-to-launch');

module.exports = Campaign => {
  Campaign.observe('before save', ctx => {
    return Promise.all([BeforeSaveSegments(ctx)]);
  });

  Campaign.observe('after save', ctx => {
    return AfterSaveSegments(Campaign, ctx).then(_ => CheckToLaunch(Campaign, ctx));
  });
};
