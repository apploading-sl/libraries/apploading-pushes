const Promise = require('bluebird');

module.exports = ctx => {
  if (ctx) {
    const data = ctx.data || ctx.instance;
    if (data) {
      if (data.languages) ctx.hookState.languages = data.languages;
      ctx.hookState.launchNow = !!data.launchNow;
    }
  }

  return Promise.resolve();
};
