const Promise = require('bluebird');
const CronJob = require('cron').CronJob;
const moment = require('moment');

const CampaignUtils = Campaign => {
  const defaultLanguage = 'es';
  return { sendCampaign, loadCronJob };

  function sendCampaign(id) {
    return Campaign.findById(id, { include: 'segments' })
      .then(campaign => {
        const data = campaign ? JSON.parse(JSON.stringify(campaign)) : {};
        if (!data.active || !data.segments) return Promise.resolve();
        const { Device } = Campaign.app.models;
        const sendExpo = require('../../models/device/remotes/sendExpo');
        return Promise.each(data.segments, ({ language, title: alertMessage }) => {
          const where = { enabled: true };
          if (language === defaultLanguage) where.or = [{ language: null }, { language }];
          else where.language = language;
          return Device.find({ where, fields: ['amazonArn'] }).then(devices => {
            return devices.length
              ? sendExpo({ devices, notificationData: {}, alertMessage })
              : Promise.resolve();
          });
        }).then(_ => campaign);
      })
      .then(campaign => {
        const data = { isLaunched: true, launchedByAdmin: moment().format() };
        return campaign.updateAttributes(data);
      });
  }

  function loadCronJob(campaign) {
    const { app } = Campaign;
    const campaignDate = moment.utc(campaign.dateAt).toDate();
    if (moment.utc().isSameOrBefore(moment(campaignDate), 'minute')) {
      if (app.activeCrons && app.activeCrons[campaign.id]) app.activeCrons[campaign.id].stop();
      const job = new CronJob(campaignDate, () => sendCampaign(campaign.id));
      job.start();
      if (!app.activeCrons) app.activeCrons = {};
      app.activeCrons[campaign.id] = job;
    }

    return Promise.resolve();
  }
};

module.exports = CampaignUtils;
