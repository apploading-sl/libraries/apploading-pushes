'use strict';

module.exports = function(Notification) {

  Notification.sendPushNotification = (userIds, text, historyId, notificationData, next) => {
    const PushUtils = require("../models-utils/PushUtils")(Notification.app.models.Device);
    PushUtils.sendPushNotification({
      userIds: userIds,
      text: text,
      historyId: historyId,
      notificationData: notificationData
    }, next);
  };

  Notification.sendTopicNotification = (title, language, notificationData, next) => {
    const PushUtils = require("../models-utils/PushUtils")(Notification.app.models.Device);
    PushUtils.sendTopicNotification({
        title: title,
        language: language,
        message: notificationData
    }, next);
  };

  // Registering Remote Methods
  Notification.remoteMethod('sendPushNotification', {
    accepts: [
      {arg: 'userIds', type: 'array', required: true},
      {arg: 'text', type: 'string', required: true},
      {arg: 'historyId', type: 'number', required: false},
      {arg: 'notificationData', type: 'object', required: false}
    ],
    http: {path: '/sendPushNotification', verb: 'post'}
  });

  Notification.remoteMethod('sendTopicNotification', {
    accepts: [
      {arg: 'title', type: 'string', required: true},
      {arg: 'language', type: 'string', required: true},
      {arg: 'notificationData', type: 'object', required: false}
    ],
    http: {path: '/sendTopicNotification', verb: 'post'}
  });
};
