// Hooks
const Hooks = require('./campaign/hooks');
const RefillCampaigns = require('./campaign/refill-campaigns');

module.exports = function (Campaign) {
  Hooks(Campaign);
  RefillCampaigns(Campaign);
};
