'use strict';

const utils = require('../utils')();

const constants = require('../constants.json');
const send = require('./device/remotes/send');

module.exports = Device => {
  Device.send = send(Device);

  const excludedRemoteMethods = [
    'get',
    'create',
    'delete',
    'findById',
    'updateById',
    'destroyById',
    'count',
  ];

  ['notifications', 'users'].forEach(function (modelName) {
    excludedRemoteMethods.forEach(function (rm) {
      Device.disableRemoteMethodByName(`prototype.__${rm}__${modelName}`);
    });
  });

  // Get Devices in DataBase from User
  Device.getUsersDevices = (userIds, cb) => {
    Device.find(
      {
        where: {
          userId: { inq: userIds },
          enabled: true,
        },
      },
      (err, results) => {
        cb(null, JSON.parse(JSON.stringify(results)));
      }
    );
  };

  // Subscribe device in APN application
  Device.subscribe = (
    userId = null,
    token,
    platform,
    appVersion,
    amazonArn,
    language,
    topicName,
    cb
  ) => {
    const DeviceUtils = require('../models-utils/DeviceUtils')(Device.app);

    if (!token || !platform)
      return cb(utils.returnError(400, 'Missing params'), null);
    let device = {
      token,
      userId,
      platform,
      appVersion,
      amazonArn,
      unreadNotifications: 0,
      enabled: 1,
      language,
    };
    if (!device.amazonArn) {
      Device.findOne({ where: { token: token } }, (err, firstDevice) => {
        if (!firstDevice)
          DeviceUtils.createPlatformEndpoint(device, (error, device) =>
            doCallback(error, device, cb)
          );
        else
          DeviceUtils.updatePlatformEndpoint(
            token,
            firstDevice.amazonArn,
            device,
            (error, device) => doCallback(error, device, cb)
          );
      });
    } else
      DeviceUtils.updatePlatformEndpoint(
        token,
        amazonArn,
        device,
        (error, device) => doCallback(error, device, cb)
      );

    function doCallback(error, device, cb) {
      if (error)
        cb(
          utils.returnError(
            422,
            'Error al crear el endpoint para el dispositvo'
          ),
          null
        );
      else {
        if (device.platform == 'expo') {
          cb(null, device);
        } else {
          checkApplicationTopics(device.amazonArn, language, topicName, () =>
            cb(null, device)
          );
        }
      }
    }

    function checkApplicationTopics(arn, language, topicName, next) {
      if (language) {
        const CampaignUtils = require('../models-utils/CampaignUtils')(Device);
        CampaignUtils.subscribeArnTopicFromLanguage(arn, language, next);
      } else if (topicName) {
        const CampaignUtils = require('../models-utils/CampaignUtils')(Device);
        CampaignUtils.subscribeArnTopicFromName(arn, topicName, next);
      } else next();
    }
  };

  // Send Push Notification for unique Device
  Device.notify = function (
    device,
    alertMessage,
    notificationData,
    historyId,
    testMode = false,
    cb
  ) {
    const endpointArn = device.amazonArn;
    const DeviceUtils = require('../models-utils/DeviceUtils')(Device.app);

    if (!endpointArn || !alertMessage)
      return cb(utils.returnError(400, 'Missing params'), null);
    if (testMode) {
      DeviceUtils.createNotification(
        endpointArn,
        constants.NOTIFICATION_STATUS.DELIVERED,
        historyId,
        (err, notificationId) => cb(null, notificationId)
      );
    } else {
      Device.send({ device, alertMessage, notificationData })
        .then(data => {
          DeviceUtils.createNotification(
            endpointArn,
            constants.NOTIFICATION_STATUS.DELIVERED,
            historyId,
            (err, notificationId) => cb(null, notificationId)
          );
        })
        .catch(err => {
          if (
            err.code &&
            err.code === constants.AMAZON_ERROR_CODE.ENDPOINT_DISABLED
          ) {
            DeviceUtils.setDeviceDisabled(endpointArn);
          }
          DeviceUtils.createNotification(
            endpointArn,
            constants.NOTIFICATION_STATUS.FAILED,
            historyId,
            cb
          );
        });
    }
  };

  // Declare Remote Method
  Device.remoteMethod('subscribe', {
    accepts: [
      { arg: 'userId', type: 'number' },
      { arg: 'token', type: 'string' },
      { arg: 'platform', type: 'string' },
      { arg: 'appVersion', type: 'string' },
      { arg: 'amazonArn', type: 'string', required: false },
      { arg: 'language', type: 'string', required: false },
      { arg: 'topicName', type: 'string', required: false },
    ],
    http: { path: '/subscribe', verb: 'post' },
    returns: { type: 'object', root: true },
  });

  Device.remoteMethod('getUsersDevices', {
    accepts: [{ arg: 'userIds', type: 'array' }],
    http: { path: '/getUsersDevices', verb: 'get' },
    returns: { type: 'object', root: true },
  });

  // Declare Remote Method
  Device.remoteMethod('notify', {
    accepts: [
      { arg: 'endpointArn', type: 'string', required: true },
      { arg: 'alertMessage', type: 'string', required: true },
      { arg: 'notificationData', type: 'object', required: false },
      { arg: 'historyId', type: 'number', required: false },
      { arg: 'testMode', type: 'boolean', required: false },
    ],
    http: { path: '/notify', verb: 'post' },
  });
};
