const Utils = () => {
  return {
    returnError: returnError,
    executeSQL: executeSQL
  };

  function returnError(statusCode, description, code) {
    let error = new Error(description);
    error.statusCode = statusCode;
    if (code) error.name = code;
    return error;
  }

  function executeSQL(app, sql, cb) {
    app.dataSources.db.connector.execute(sql, function (err, rows) {
      if (err) {
        console.log('Error in query: ', sql);
        throw err;
      }

      if(cb) cb(null, rows);
    });
  }

};


module.exports = Utils;
