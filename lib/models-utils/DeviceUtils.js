'use strict';

const utils = require('../utils')();

function DeviceUtils(app) {
  const Device = app.models.Device;

  return {
    createPlatformEndpoint: createPlatformEndpoint,
    updatePlatformEndpoint: updatePlatformEndpoint,
    setDeviceDisabled: setDeviceDisabled,
    setDeviceEnabled: setDeviceEnabled,
    createNotification: createNotification,
  };

  function insertOrUpdateData(deviceItem, cb) {
    Device.findOne(
      {
        where: { amazonArn: deviceItem.amazonArn },
      },
      (err, firstDevice) => {
        if (err) throw err;
        if (!firstDevice) {
          insertData(deviceItem, (err, data) => {
            if (err) cb(err, null);
            else cb(null, deviceItem);
          });
        } else {
          updateData(firstDevice, deviceItem, (err, data) => {
            if (err) cb(err, null);
            else cb(null, firstDevice);
          });
        }
      }
    );
  }

  function insertData(deviceItem, cb) {
    Device.create(
      {
        token: deviceItem.token,
        userId: deviceItem.userId,
        platform: deviceItem.platform,
        appVersion: deviceItem.appVersion,
        amazonArn: deviceItem.amazonArn,
        unreadNotifications: deviceItem.badge,
        enabled: deviceItem.enabled,
        language: deviceItem.language
      },
      (err, device) => {
        if (err) cb(err, null);
        else cb(null, device);
      }
    );
  }

  function updateData(deviceObject, deviceData, cb) {
    Device.updateAll(
      { amazonArn: deviceData.amazonArn },
      deviceData,
      (err, info) => {
        cb(err, deviceObject);
      }
    );
  }

  function checkTokenEndpoint(newToken, amazonArn, cb) {
    if (!amazonArn) return cb(null, false);

    Device.findOne({ where: { amazonArn: amazonArn } }, (err, firstDevice) => {
      if (!firstDevice) cb(null, false);
      else cb(null, firstDevice.token == newToken);
    });
  }

  function updateTokenEndpoint(token, amazonArn, cb) {
    app.sns.setEndpointAttributes(
      {
        Attributes: { Token: token },
        EndpointArn: amazonArn,
      },
      cb
    );
  }

  function createPlatformEndpoint(device, cb) {
    let platformApplicationArn =
      device.platform.toLowerCase() === 'ios'
        ? app.PlatformApplicationArnIOS
        : app.PlatformApplicationArnAndroid;
    if (device.platform != 'expo') {
      app.sns.createPlatformEndpoint(
        {
          PlatformApplicationArn: platformApplicationArn,
          Token: device.token,
        },
        function (err, data) {
          if (err) {
            cb(err, null);
          } else if (data.EndpointArn) {
            let deviceItem = {
              token: device.token,
              userId: device.userId,
              platform: device.platform,
              appVersion: device.appVersion,
              amazonArn: data.EndpointArn,
              unreadNotifications: 0,
              enabled: 1,
            };

            insertOrUpdateData(deviceItem, function (err, device) {
              if (err) throw err;

              if (err)
                cb(utils.returnError(422, 'Error creating device'), null);
              else {
                cb(null, device);

                // check arn enable in background after return cb
                checkARNEnabled(device.amazonArn, function (err, enabled) {
                  if (enabled == 'false') {
                    let endpointArn = device.amazonArn;
                    enableEndpoint(endpointArn, function (error, data) {
                    });
                  }
                });
              }
            });
          } else
            cb(
              utils.returnError(
                422,
                'No data response from createPlatformEndpoint'
              ),
              null
            );
        }
      );
    } else {
      createExpoDevice(device, cb);
    }
  }

  function updatePlatformEndpoint(token, amazonArn, device, cb) {
    if (device.platform == 'expo') {
      device.amazonArn = token;
      updateData(device, device, cb);
    } else {
    checkTokenEndpoint(token, amazonArn, function (err, isSame) {
      if (!isSame) updateData(device, device, cb);
      else {
        updateTokenEndpoint(token, amazonArn, function (err, data) {
          if (err)
            cb(
              utils.returnError(
                422,
                'El token ha sido modificado. Error al actualizar el token de AWS SNS'
              ),
              null
            );
          else {
            device.token = token;
            device.amazonArn = amazonArn;
            updateData(device, device, cb);
          }
        });
      }
    });
  }
  }

  function enableEndpoint(amazonArn, cb) {
    app.sns.setEndpointAttributes(
      {
        Attributes: { Enabled: 'true' },
        EndpointArn: amazonArn,
      },
      (err, data) => {
        if (err) setDeviceDisabled(amazonArn);
        else setDeviceEnabled(amazonArn);
        cb(err, null);
      }
    );
  }

  function checkARNEnabled(amazonArn, cb) {
    app.sns.getEndpointAttributes(
      {
        EndpointArn: amazonArn,
      },
      (err, data) => {
        if (err) cb(err, null);
        else cb(null, data.Attributes.Enabled);
      }
    );
  }

  function setDeviceDisabled(amazonArn) {
    Device.updateAll({ amazonArn: amazonArn }, { enabled: false }, null);
  }

  function setDeviceEnabled(amazonArn) {
    Device.updateAll({ amazonArn: amazonArn }, { enabled: true }, null);
  }

  function createNotification(amazonArn, statusId, historyId, cb) {
    Device.findOne({ where: { amazonArn: amazonArn } }, (err, device) => {
      if (err) throw err;

      if (device) {
        app.models.Notification.create(
          {
            deviceId: device.id,
            statusId: statusId,
            historyId: historyId,
          },
          (err, notification) => {
            if (err) throw err;
            cb(null, notification.id);
          }
        );
      } else
        cb(
          utils.returnError(
            422,
            `Device with amazonArn ${amazonArn} not found. Can not send push notification`
          ),
          null
        );
    });
  }

  function createExpoDevice(device, next) {
    device.amazonArn = device.token;
    insertOrUpdateData(device, next);
  }
}
module.exports = DeviceUtils;
