'use strict';

const PushUtils = require('./PushUtils');
const utils = require('../utils')();

function CampaignUtils(Campaign) {
  const app = Campaign.app;

  return {
    createTopic,
    subscribeArnTopicFromLanguage,
    subscribeArnTopicFromName,
    getArnTopicFromLanguage,
    getArnFromTopicName,
    topicPublish
  };

  function createTopic(name, next) {
    if (!name) next(utils.returnError(400, 'Missing name field'), null);
    else {
      app.sns.createTopic({ Name: name }, (err, data) => {
        if (err) next(new Error(JSON.stringify(err.stack)), null);
        else next(null, data.TopicArn);
      })
    }
  }

  function getArnTopicFromLanguage(language, next) {
    getTopics(language, (topics) => {
      let topicExists = null;
      if (topics.length) topicExists = topics.find(topic => topic.TopicArn.split(':').pop().split('-')[1] === language);
      next(null, topicExists ? topicExists.TopicArn : null);
    });
  }

  function subscribeArnTopicFromLanguage(deviceArn, language, next) {
    getTopics(language, (topics) => {
      if (topics.length) {
        let topicExists = topics.find(topic => topic.TopicArn.split(':').pop().split('-')[1] === language);
        if (typeof topicExists === 'undefined') next(new Error('Cannot find Topic with this language'), null);
        else {
          let topicArn = topicExists.TopicArn;
          if (topicArn) {
            app.sns.subscribe({
              Protocol: 'application',
              TopicArn: topicArn,
              Endpoint: deviceArn
            }, (err, data) => {
              next();
            });
          } else next();
        }
      } else next();
    });
  }

  function subscribeArnTopicFromName(deviceArn, name, next) {
    getTopics(name, (topics) => {
      if (topics.length) {
        let topicExists = topics.find(topic => topic.TopicArn.split(':').pop() === name);
        if (typeof topicExists === 'undefined') next(new Error('Cannot find Topic with this name'), null);
        else {
          let topicArn = topicExists.TopicArn;
          if (topicArn) {
            app.sns.subscribe({
              Protocol: 'application',
              TopicArn: topicArn,
              Endpoint: deviceArn
            }, (err, data) => {
              next();
            });
          } else next();
        }
      } else next();
    });
  }

  function getArnFromTopicName(name, next) {
    getTopics(name, (topics) => {
      let topicExists = topics.find(topic => topic.TopicArn.split(':').pop() === name);
      next(null, topicExists ? topicExists.TopicArn : null);
    });
  }

  function getTopics(field, next) {
    if (!field) next(utils.returnError(400, 'Missing field'), null);
    else {
      app.sns.listTopics((err, data) => {
        if (err) next(new Error(JSON.stringify(err.stack)), null);
        else next(data.Topics);
      })
    }
  }

  function topicPublish(topicArn, alertMessage, notificationData, cb) {
    if (!topicArn || !alertMessage) cb(utils.returnError(400, 'Missing params'), null);
    else {
      app.sns.publish({
        Message: PushUtils().createPayload(alertMessage, notificationData),
        MessageStructure: 'json',
        TopicArn: topicArn,
      }, cb);
    }
  }
}

module.exports = CampaignUtils;
