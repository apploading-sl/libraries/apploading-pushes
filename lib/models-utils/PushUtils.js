'use strict';

const async = require('async');
const utils = require('../utils')();

function PushUtils(Device) {
  return { sendPushNotification, createPayload };

  function sendPushNotification(args = {}, cb) {
    if (args.text && typeof args.text === 'string' && args.userIds && args.userIds.length) {
      Device.getUsersDevices(args.userIds, (error, devices) => {
        if (error) return cb(error);

        if (devices.length) {
          async.map(
            devices,
            (device, nextDevice) => {
              Device.notify(
                device,
                args.text,
                args.notificationData,
                args.historyId,
                args.testMode,
                (err, notificationId) => {
                  process.nextTick(() => {
                    nextDevice(null, notificationId);
                  });
                }
              );
            },
            (err, results) => {
              if (err) cb(utils.returnError(400, 'No han terminado todos los pushes'), null);
              else cb(err, results);
            }
          );
        } else cb(utils.returnError(404, 'No hay devices donde enviar los pushes'), null);
      });
    } else cb(utils.returnError(400, 'JSON required with correct params'), null);
  }

  function createPayload(alertMessage, notificationData, device) {
    let iosPayload = JSON.stringify({
      aps: {
        alert: alertMessage,
        sound: 'default',
        badge: 1,
      },
      notificationData: notificationData,
    });

    let androidPayload = createAndroidPayload(alertMessage, notificationData, device);

    return JSON.stringify({
      default: alertMessage,
      APNS: iosPayload,
      APNS_SANDBOX: iosPayload,
      GCM: androidPayload,
    });

    function createAndroidPayload(alertMessage, notificationData, device) {
      let payload = null;
      if (useNewPayload(notificationData, device)) {
        const data = JSON.parse(JSON.stringify(notificationData));
        data.message = alertMessage;
        payload = { data };
        delete payload.data.useNewPayloadOnAndroid;
      } else {
        payload = {
          notification: {
            body: alertMessage,
            sound: 'default',
          },
          data: notificationData,
        };
      }
      return JSON.stringify(payload);

      function useNewPayload(notificationData, device) {
        return (
          notificationData.useNewPayloadOnAndroid &&
          parseInt(notificationData.useNewPayloadOnAndroid.appVersion) <=
            parseInt(device.appVersion) &&
          notificationData.useNewPayloadOnAndroid.platform == device.platform
        );
      }
    }
  }
}

module.exports = PushUtils;
